import * as React from 'react';

import Layout from '@/components/layout/Layout';
import Seo from '@/components/Seo';

/**
 * SVGR Support
 * Caveat: No React Props Type.
 *
 * You can override the next-env if the type is important to you
 * @see https://stackoverflow.com/questions/68103844/how-to-override-next-js-svg-module-declaration
 */
import {useEffect, useState} from "react";
import {UserCollection} from "@/services/user";

// !STARTERCONF -> Select !STARTERCONF and CMD + SHIFT + F
// Before you begin editing, follow all comments with `STARTERCONF`,
// to customize the default configuration.
const employees =  [
  {
    name: "Vito Corleone",
    popularity: 5,
    biography: "Vito Andolini's story starts in Corleone, Sicily. In 1901, the local mafia chieftain, Don Ciccio, murders Vito's father Antonio when he refuses to pay him tribute. Paolo, Vito's older brother, swears revenge, but Ciccio's men kill him too. Ciccio then sends his men to fetch nine-year old Vito. However, Vito's mother insists on going as well and begs Ciccio to spare Vito. Ciccio refuses, reasoning the boy will seek revenge as a grown man. Upon Ciccio's refusal, Vito's mother holds a knife to Ciccio's throat, allowing her son to escape while Ciccio's men kill her. Family friends smuggle Vito out of Sicily, putting him on a ship with immigrants traveling to America. Ellis Island immigration officials rename him Vito Corleone, using his village for his surname. He later uses Andolini as his middle name in acknowledgement of his family heritage.",
    image: "Vito Corleone.jpg",
    colleagues: ["Carlo Rizzi", "Luci Mancini"]
  },
  {
    name: "Carmelia Corleone",
    popularity: 2,
    biography: "Carmela was born in Sicily Italy in 1897, and emigrated to the United States shortly after the turn of the century. She married Vito Corleone in 1914; they were married for just over 40 years until Vito's death in 1955. They had four children – Sonny, Fredo, Michael and Connie. They also took in Sonny's friend Tom Hagen, who later served as the family consigliere. In the novel, Carmela Corleone is portrayed as a traditional Italian immigrant woman who speaks in very broken English. In the movies, however, she speaks fluent English as an adult, with a marked New York accent. In the novel, she develops a close relationship with Michael's girlfriend and future wife, Kay. She is given more expansive dialogue in The Godfather Part II, notably when she confronts her daughter Connie about her behavior early in the film, and when she discusses family life with Michael, who fears that his role as Don of the Corleone criminal empire will cost him his family. Carmela Corleone dies toward the end of the sequel.",
    image: "Carmelia Corleone.jpg",
    colleagues: []
  },
  {
    name: "Carlo Rizzi",
    popularity: 5,
    biography: "A half northern Italian, half Sicilian native of Nevada and former labourer, Rizzi migrated to New York City following trouble with the law and became a friend of Sonny Corleone, through whom he met Sonny's sister Connie in 1941 at a surprise birthday party for Sonny's father Vito. They were married on the last Saturday of August 1945 in a traditional Italian wedding at the Corleone mall, a compromise made in order to appease Vito, who was disappointed with his daughter's choice in a husband.",
    image: "Carlo Rizzi.jpg",
    colleagues: ["Vito Corleone", "Carmelia Corleone"]
  },
  {
    name: "Luci Mancini",
    popularity: 5,
    biography: "In the novel, Lucy is a fairly important supporting character, with several chapters dedicated to her story. After Sonny's death, Vito's consigliere, Tom Hagen sends Lucy to Las Vegas. She is given a small interest (five and later ten) in one of the family's hotels, primarily so that she can keep an eye on Vito's middle son, Fredo, who is learning the hotel and casino business. She also serves as a shareholder-of-record who has no criminal record: several such owners are necessary for a valid gaming license. On paper she is a millionaire, although she does not vote her shares in the casinos.",
    image: "Luci Mancini.jpg",
    colleagues: ["Carmelia Corleone"]
  }
]
const data = new UserCollection(employees);
export default function HomePage() {

  const [selectedId, setSelectedId] = useState<string>(data.list[0]);
  const [range, setRange] = useState<number>(0)

  useEffect(() => {
    setRange(data.entities[selectedId].popularity)
  }, [selectedId])
  const handleSelectUser = (event: React.MouseEvent<HTMLElement>) => {
    const userId = event.currentTarget.getAttribute('data-name');
    setSelectedId(userId ||'');
  }

  const handleChangeRange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value;
    setRange(+value);
  }
  return (
    <Layout>
      {/* <Seo templateTitle='Home' /> */}
      <Seo />

      <main>
        <section className='bg-white'>
          <div className='relative bg-cover bg-no-repeat bg-left-top bg-wrapper-bg max-w-max flex min-h-screen grid grid-cols-1 md:grid-cols-12 items-center justify-center text-center gap-y-8 md:gap-y-0'>
            <aside className='relative z-20 col-span-4 bg-black bg-opacity-70 h-full'>
              <div className='flex justify-center w-full flex-col'>
                <img className='p-8 mt-8 w-64 mx-auto' alt='logo' src='/svg/the-godfather.svg' />
                <div className='flex flex-col'>
                  {data.list.map((id) => {
                    return (
                        <p data-name={id} onClick={handleSelectUser} className={'text-white cursor-pointer hover:bg-gray-700 ' + `${id === selectedId ? 'bg-gray-700': ''}`} key={id}>{data.entities[id].name}</p>
                    )
                  })}
                </div>
              </div>
            </aside>
            <main className='relative z-20 col-span-8'>
              <figure className='grid grid-cols-1 md:grid-cols-6 text-left text-white px-4 md:px-16 gap-4'>
                <img className='col-span-2 w-full rounded-md border border-white' alt={data.entities[selectedId].name} src={'/images/profiles/' + data.entities[selectedId].image} />
                <div className='col-span-4'>
                  <div className='flex space-y-4 flex-col'>
                    <div className='block'>
                      <span className='h-auto' style={{fontSize: range+'rem'}}>{data.entities[selectedId].name}</span>
                    </div>
                    <div className='flex flex-col md:flex-row md:space-x-4 md:items-center'>
                      <p>Popularity</p>
                      <input id="default-range" type="range" min="0" max="10" value={range} onChange={handleChangeRange}
                             className="w-full h-2 bg-gray-200 rounded-lg appearance-none cursor-pointer" />

                    </div>
                    <div className='rounded-md bg-black p-4'>
                      <h5>Biography</h5>
                      <p>{data.entities[selectedId].biography}</p>
                    </div>
                  </div>
                </div>
              </figure>

            </main>
            <div className='col-span-full bg-gray-700 h-3/5 absolute bottom-0 left-0 z-10 w-full'>

            </div>
          </div>
        </section>
      </main>
    </Layout>
  );
}
