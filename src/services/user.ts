type TUser = {
  userId?: string;
  name: string;
  popularity: number;
  biography: string;
  image: string;
  colleagues?: string[];
};
class UserEntity {
  userId = '';
  name = '';
  popularity = 0;
  biography = '';
  image = '';
  colleagues: string[] = [];
  constructor(data: TUser) {
    this.userId = data?.userId ?? '';
    this.name = data?.name ?? '';
    this.popularity = data?.popularity || 0;
    this.biography = data?.biography ?? '';
    this.image = data?.image ?? '';
    this.colleagues = data?.colleagues || [];
  }
}
export class UserCollection {
  list: string[] = [];
  entities = {} as { [key: string]: TUser };
  total = 0;
  constructor(data: TUser[]) {
    this.parseResponse(data);
  }
  parseResponse(data: TUser[]): void {
    this.total = data.length;
    (data ?? []).map((user, index) => {
      const userId = 'id' + index + 1;
      this.list.push(userId);
      this.entities[userId] = new UserEntity({ ...user, userId });
    });
  }
}
